from turtle import *

# Function that draws all the emojis together
def sentence():
    eye()
    penup()
    goto(85,-12)
    down()
    left(135)
    heart()
    
# Function that draws the eye
def eye():
    iris()
    eyelids()
    shine()
    
# Function that draws the iris of the eye emoji
def iris():
    color("black")
    begin_fill()
    circle(15,360)
    color("blue")
    end_fill()
    
# Function that draws the eyelids of the eye emoji
def eyelids():
    color("black")
    up()
    goto(-35,15)
    down()
    seth(-45)
    circle(50,90)
    seth(135)
    circle(50,90)
   
# Function that draws the reflection of light in the eye emoji
def shine():
    up()
    goto(-9,25)
    down()
    begin_fill()
    color("white")
    circle(5,360)
    end_fill()

# Function that draws the whole heart emoji
def heart():
	right_side()
	right_loop()
	left_loop()
	left_side()

# Function that draws the right side of the heart emoji
def right_side():
	begin_fill()
	color("black")
	left(55)
	forward(60)
	left(45)
    
# Function that draws the half circle on the right of the heart emoji
def right_loop():
	for i in range(140):
		forward(0.4)
		left(1.25)
	right(187)
   
# Function that draws the half circle on the left of the heart emoji
def left_loop():
	for i in range(160):
		forward(0.36)
		left(1.2)

# Function that draws the left side of the heart emoji
def left_side():
	left(32)
	forward(60)
	color("red")
	end_fill()

# Draws the eye and heart emojis 
sentence()
		
done()
